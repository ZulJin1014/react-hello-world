import * as types from '../actions/newsRuducersTypes';

export default function newsReducer(state = { sources: [] }, action) {
  switch (action.type) {
    case types.ADD_NEWS: {
      if (!action.res.length) {
        return {
          sources: state.sources.concat(
            {
              name: action.source,
              articles: action.res.articles,
              checked: true,
            },
          ),
        };
      }
      break;
    }
    case types.INVERT_VISIBLE: {
      const index = state.sources
        .findIndex(source => source.name === action.source);
      return {
        sources: state.sources
          .slice(0, index)
          .concat({
            ...state.sources[index],
            checked: !state.sources[index].checked,
          })
          .concat(...state.sources.slice(index + 1, state.sources.length)),
      };
    }
    default:
  }
  return state;
}
