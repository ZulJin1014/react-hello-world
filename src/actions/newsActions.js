import * as types from './newsRuducersTypes';

function getNewsFromSource(source) {
  // eslint-disable-next-line max-len
  const URL = `https://newsapi.org/v1/articles?source=${source}&apiKey=ab2ad3aa390d4b40ad962894df07b5f6`;
  return fetch(URL)
    .then(result => result.json())
    .then(json => {
      if (json.articles) {
        return {
          source,
          articles: json.articles,
        };
      }
      return [];
    });
}

export function addNewsAction(source) {
  return {
    type: types.ADD_NEWS,
    source,
    promise: getNewsFromSource(source),
  };
}

export function invertVisible(source) {
  return {
    type: types.INVERT_VISIBLE,
    source,
  };
}
