import React, { Component } from 'react';
import './Article.css';

export default class Article extends Component {
  static propTypes = {
    data: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  readmoreClick = () => {
    this.setState({ visible: true });
  };

  render() {
    return (
      <div className="article">
        <a href={this.props.data.url} className="news_title" rel="noopener noreferrer" target="_blank">{this.props.data.title}</a>
        <img src={this.props.data.urlToImage} alt="news" />
        <p className="news_text">{this.props.data.description}</p>
      </div >
    );
  }
}
