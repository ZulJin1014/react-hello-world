import 'whatwg-fetch';
import { createStore, applyMiddleware } from 'redux';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import News from '../News/News';
import LeftMenu from '../LeftMenu/LeftMenu';
import './App.css';
import * as actions from '../../actions/newsActions';
import newsReducer from '../../reducers/newsReducer';
import promiseMiddleware from '../../middlewares/promiseMiddleware';

const store = applyMiddleware(promiseMiddleware)(createStore)(newsReducer);
const sources = ['techcrunch', 'abc-news-au', 'bbc-news'];
class App extends Component {
  componentWillMount() {
    sources.forEach((source) => {
      store.dispatch(actions.addNewsAction(source));
    });
  }

  render() {
    return (
      <Provider store={store}>
        <div className="app" >
          <LeftMenu />
          <News />
        </div >
      </Provider>
    );
  }
}

export default App;
