import React, { Component } from 'react';
import { connect } from 'react-redux';
import shortid from 'shortid';
import Article from '../Article';
import './News.css';

const renderNews = (sources) => {
  if (!sources.length) {
    return [];
  }
  const articles = [];
  sources
    .filter(source => source.articles && source.articles.length
      && source.checked)
    .forEach((source) => {
      articles.push(source.articles.map((data) => (
        <div key={shortid.generate()}>
          <Article data={data} />
        </div>
      )));
    });
  return articles;
};

class News extends Component {
  static propTypes = {
    sources: React.PropTypes.array.isRequired,
  };



  render() {
    const listNews = renderNews(this.props.sources);

    return (
      <div className="news">
        {listNews}
        <strong className={listNews.length > 0 ? '' : 'none'}>
          Общее количество новостей: {listNews.length}
        </strong>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    sources: state.sources,
  };
}

const connectedNews = connect(mapStateToProps)(News);
export default connectedNews;
