import React, { Component } from 'react';
import shortid from 'shortid';
import { connect } from 'react-redux';
import './LeftMenu.css';
import * as actions from '../../actions/newsActions';

class LeftMenu extends Component {


  static propTypes = {
    sources: React.PropTypes.array.isRequired,
    invertVisible: React.PropTypes.func.isRequired,
    allCount: React.PropTypes.number.isRequired,
  }

  renderSourceList(sources) {
    return sources.map((source) =>
      <li key={shortid.generate()} className="menu-element">
        <input type="checkbox" checked={source.checked} onChange={this.props.invertVisible(source.name)} />
        <span>{source.name}</span>
      </li>);
  }

  render() {
    this.listSources = this.renderSourceList(this.props.sources);
    return (
      <div className="left-menu">
        <ul>
          {this.listSources}
        </ul>
        <span className="all-count">Всего новостей: {this.props.allCount}</span>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const filteredSources = state.sources
    .filter(source => source.articles && source.articles.length);
  return {
    sources: filteredSources,
    allCount: filteredSources.filter(source => source.checked).reduce(
      ((previous, next) => previous + next.articles.length), 0),
  };
};

const mapDispatchToProps = (dispatch) => ({
  invertVisible: (source) => () => dispatch(actions.invertVisible(source)),
});

const connectedLeftMenu =
  connect(mapStateToProps, mapDispatchToProps)(LeftMenu);

export default connectedLeftMenu;


